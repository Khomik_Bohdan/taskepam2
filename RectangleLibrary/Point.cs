﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RectangleLibrary
{
    public class Point
    {
        public double X { get; private set; }
        public double Y { get; private set; }

        public Point(double x = 0, double y = 0)
        {
            X = x;
            Y = y;
        }
    }
}

