﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RectangleLibrary
{
    public enum Direction
    {
        Right,
        Down,
        Left,
        Up
    }

    public class Rectangle
    {
        public Point RightTop { get; private set; }
        public Point RightBottom { get; private set; }
        public Point LeftBottom { get; private set; }
        public Point LeftTop { get; private set; }
        /// <summary>
        /// Parameterless constructor, initializes fields by 0
        /// </summary>
        public Rectangle()
        {
            RightTop = new Point(0, 0);
            RightBottom = new Point(0, 0);
            LeftBottom = new Point(0, 0);
            LeftTop = new Point(0, 0);
        }

        /// <summary>
        /// Constructor, that builds rectangle using right top and left bottom points
        /// </summary>
        /// <param name="rightTop">Right top point</param>
        /// <param name="leftBottom">Left bottom point</param>
        public Rectangle(Point rightTop, Point leftBottom)
        {
            if (rightTop == null || leftBottom == null)
                throw new ArgumentNullException("Can not use null parameters");
            if ((rightTop.X >= leftBottom.X) && (rightTop.Y >= leftBottom.Y))
            {
                RightTop = rightTop;
                RightBottom = new Point(rightTop.X, leftBottom.Y);
                LeftBottom = leftBottom;
                LeftTop = new Point(leftBottom.X, rightTop.Y);
            }
            else
                throw new ArgumentException("Given point can not be used for building a rectangle!");
        }

        /// <summary>
        /// Constructor that builds rectangle using four given points
        /// </summary>
        /// <param name="p1">Right top point</param>
        /// <param name="p2">Right bottom point</param>
        /// <param name="p3">Left bottom point</param>
        /// <param name="p4">Left top point</param>
        public Rectangle(Point p1, Point p2, Point p3, Point p4)
        {
            if ((p1 == null) || (p2 == null) || (p3 == null) || (p4 == null))
                throw new ArgumentNullException("Can not use null parameters");
            if (IsValid(p1, p2, p3, p4))
            {
                RightTop = p1;
                RightBottom = p2;
                LeftBottom = p3;
                LeftTop = p4;
            }
            else
                throw new ArgumentException("Given point can not be used for building a rectangle!");
        }

        /// <summary>
        /// Move rectangle in the given direction
        /// </summary>
        /// <param name="direction">Movement direction</param>
        /// <param name="d">Distance to move</param>
        /// <returns>New rectangle</returns>
        public Rectangle Move(Direction direction, double d)
        {
            Point rightTop = RightTop;
            Point rightBottom = RightBottom;
            Point leftTop = LeftTop;
            Point leftBottom = LeftBottom;
            switch (direction)
            {
                case Direction.Right:
                    rightTop = new Point(RightTop.X + d, RightTop.Y);
                    rightBottom = new Point(RightBottom.X + d, RightBottom.Y);
                    leftTop = new Point(LeftTop.X + d, LeftTop.Y);
                    leftBottom = new Point(LeftBottom.X + d, LeftBottom.Y);
                    break;
                case Direction.Down:
                    rightTop = new Point(RightTop.X, RightTop.Y - d);
                    rightBottom = new Point(RightBottom.X, RightBottom.Y - d);
                    leftTop = new Point(LeftTop.X, LeftTop.Y - d);
                    leftBottom = new Point(LeftBottom.X, LeftBottom.Y - d);
                    break;
                case Direction.Left:
                    rightTop = new Point(RightTop.X - d, RightTop.Y);
                    rightBottom = new Point(RightBottom.X - d, RightBottom.Y);
                    leftTop = new Point(LeftTop.X - d, LeftTop.Y);
                    leftBottom = new Point(LeftBottom.X - d, LeftBottom.Y);
                    break;
                case Direction.Up:
                    rightTop = new Point(RightTop.X, RightTop.Y + d);
                    rightBottom = new Point(RightBottom.X, RightBottom.Y + d);
                    leftTop = new Point(LeftTop.X, LeftTop.Y + d);
                    leftBottom = new Point(LeftBottom.X, LeftBottom.Y + d);
                    break;
            }
            return new Rectangle(rightTop, rightBottom, leftBottom, leftTop);
        }

        /// <summary>
        /// Check, are the given points are correct for building rectangle
        /// </summary>
        /// <param name="rightTop">Right top point</param>
        /// <param name="rightBottom">Right bottom point</param>
        /// <param name="leftBottom">Left bottom point</param>
        /// <param name="leftTop">Left top point</param>
        /// <returns>Bool value, that indicates does the points are correct</returns>
        public bool IsValid(Point rightTop, Point rightBottom, Point leftBottom, Point leftTop)
        {
            if ((rightTop == null) || (rightBottom == null) || (leftBottom == null) || (leftTop == null))
                return false;
            if ((rightTop.X != rightBottom.X) || (leftTop.X != leftBottom.X) || (rightTop.Y != leftTop.Y) || (rightBottom.Y != leftBottom.Y))
                return false;
            if ((rightTop.X < leftTop.X) || (rightBottom.X < leftBottom.X) || (rightTop.Y < rightBottom.Y) || (leftTop.Y < leftBottom.Y))
                return false;
            return true;
        }

        /// <summary>
        /// Check, does two rectangles intersects
        /// </summary>
        /// <param name="rect1">First rectangle</param>
        /// <param name="rect2">Second rectangle</param>
        /// <returns>True, if they intersects, false if not</returns>
        public static bool AreIntersected(Rectangle rect1, Rectangle rect2)
        {
            if ((rect1 == null) || (rect2 == null))
                return false;
            if ((rect1.RightTop.X <= rect2.LeftTop.X) || (rect2.RightTop.X <= rect1.LeftTop.X))
                return false;
            if ((rect1.LeftTop.Y <= rect2.LeftBottom.Y) || (rect1.LeftBottom.Y >= rect2.LeftTop.Y))
                return false;
            return true;
        }

        /// <summary>
        /// Build smallest rectangle, that contains two given rectangles
        /// </summary>
        /// <param name="rect1">First rectangle</param>
        /// <param name="rect2">Second rectangle</param>
        /// <returns>New rectangle</returns>
        public static Rectangle CreateUnion(Rectangle rect1, Rectangle rect2)
        {
            if ((rect1 == null) || (rect2 == null))
                throw new ArgumentNullException("Can not use null parameters");
            double rightX = Math.Max(rect1.RightTop.X, rect2.RightTop.X);
            double rightY = Math.Max(rect1.RightTop.Y, rect2.RightTop.Y);
            double leftX = Math.Min(rect1.LeftBottom.X, rect2.LeftBottom.X);
            double leftY = Math.Min(rect1.LeftBottom.Y, rect2.LeftBottom.Y);
            return new Rectangle(new Point(rightX, rightY), new Point(leftX, leftY));
        }

        /// <summary>
        /// Build intersection of two rectangles
        /// </summary>
        /// <param name="rect1">First rectangle</param>
        /// <param name="rect2">Second rectangle</param>
        /// <returns>New rectangle</returns>
        public static Rectangle CreateIntersection(Rectangle rect1, Rectangle rect2)
        {
            if (Rectangle.AreIntersected(rect1, rect2))
            {
                double rightX = Math.Min(rect1.RightTop.X, rect2.RightTop.X);
                double rightY = Math.Min(rect1.RightTop.Y, rect2.RightTop.Y);
                double leftX = Math.Max(rect1.LeftBottom.X, rect2.LeftBottom.X);
                double leftY = Math.Max(rect1.LeftBottom.Y, rect2.LeftBottom.Y);
                return new Rectangle(new Point(rightX, rightY), new Point(leftX, leftY));
            }
            else
                throw new ArgumentException("This rectangles are not intersected!");
        }

        /// <summary>
        /// Represents rectangle in string format
        /// </summary>
        /// <returns>String value</returns>
        public override string ToString()
        {
            return string.Format("RightTop:({0};{1}), RightBottom:({2};{3}), LeftBottom:({4};{5}), LeftTop:({6};{7})", RightTop.X, RightTop.Y, RightBottom.X, RightBottom.Y, LeftBottom.X, LeftBottom.Y, LeftTop.X, LeftTop.Y);
        }
    }
}
