﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RectangleLibrary;

namespace Task2_Rectangle
{
    class Program
    {
        static void Main(string[] args)
        {
            TestRectangleLibrary();
        }

        public static void TestRectangleLibrary()
        {
            try
            {
                Rectangle rect1 = new Rectangle();
                Rectangle rect2 = new Rectangle(new Point(5, 5), new Point(0, 0));
                Console.WriteLine("Coordinates of rect2 build on two points:");
                Console.WriteLine("Rect2:{0} \n", rect2);
                Rectangle rect3 = rect2;
                Console.WriteLine("Rect3:{0} \n", rect3);
                rect3 = rect3.Move(Direction.Right, 3);
                rect3 = rect3.Move(Direction.Up, 2);
                Console.WriteLine("Coordinates of rect3 after moving 3 points right and two points up:");
                Console.WriteLine("Rect3:{0} \n", rect3);
                Rectangle rect4 = new Rectangle(new Point(7, 7), new Point(7, 2), new Point(2, 2), new Point(2, 7));
                Console.WriteLine("Rect4:{0} \n", rect4);
                Rectangle rect5 = Rectangle.CreateUnion(rect2, rect4);
                Console.WriteLine("Rect5 represents the smallest union of rect2 and rect4");
                Console.WriteLine("Rect5:{0} \n", rect5);
                Rectangle rect6 = Rectangle.CreateIntersection(rect2, rect4);
                Console.WriteLine("Rect6 represents the intersection of rect2 and rect4");
                Console.WriteLine("Rect6:{0} \n", rect6);
                Console.WriteLine("Application ended successfully");
                Console.WriteLine("Press enter to exit");
            }
            catch (ArgumentNullException argEx)
            {
                Console.WriteLine(argEx.Message);
                Console.WriteLine("Application crashed!");
            }
            catch (ArgumentException argEx)
            {
                Console.WriteLine(argEx.Message);
                Console.WriteLine("Application crashed!");
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
